//
//  SeeReservationsViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class SeeReservationsViewController: UIViewController {

    
    //MARK: Outlets
   
    
    @IBOutlet weak var resultField: UITextView!
    
    // MARK: Firebase variables
    var db:Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // MARK: FB: get the current user (if one exists!)
        let currentUser = Auth.auth().currentUser;
        if (currentUser != nil) {
            print("We have a user!")
            print("User id: \(currentUser?.uid)")
            print("Email: \(currentUser?.email)")
        }
        else {
            print("no user is signed in.")
        }
        print("You are on the see reservations screen")
        
        db = Firestore.firestore()
        
        let settings = db.settings
        db.settings = settings
        
        db.collection("reservations").getDocuments() {
            (x, error) in
            
            if (x == nil){
                print("Error geting Document: \(error)")
                return
            }
            
            x?.documentChanges.forEach({
                (diff) in
                
                if (diff.type == DocumentChangeType.added) {
                    
                let data = diff.document.data()
                let restaurant = data["restaurant"] as! String
                let day = data["day"] as! String
                let numSeats = data["numSeats"] as! String
                    
                    print(restaurant)
                    print(day)
                    print(numSeats)
                    
                    self.resultField.text = self.resultField.text + "username: \(currentUser?.email) \n \(day): \(restaurant), \(numSeats) seats \n\n"
                }
            })
        }
        
        
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
