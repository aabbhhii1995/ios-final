//
//  LoginViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextBox: UITextField!
    @IBOutlet weak var passwordTextBox: UITextField!
    @IBOutlet weak var statusMessageLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: Actions
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        print("Pressed login button")
        
        let email = emailTextBox.text!
        let password = passwordTextBox.text!
        
        Auth.auth().signIn(withEmail: email, password: password) {
            
            (user, error) in
            
            if (user != nil) {
                
                print("User signed in! ")
                print("User id: \(user?.user.uid)")
                print("Email: \(user?.user.email)")
                
                
                self.performSegue(withIdentifier: "segueLoginSignup", sender: nil)
            }
            else {
                
                print("ERROR!")
                print(error?.localizedDescription)
                
                
                self.statusMessageLabel.text = error?.localizedDescription
            }
        }
    }
    
    
    @IBAction func signupButtonPressed(_ sender: Any) {
        print("pressed signup button")
        
        let email = emailTextBox.text!
        let password = passwordTextBox.text!

        Auth.auth().createUser(withEmail: emailTextBox.text!, password: passwordTextBox.text!) {
            
            (user, error) in
            
            if (user != nil) {
                // 1. New user created!
                
                print("User id: \(user?.user.uid)")
                print("Email: \(user?.user.email)")
                
            }
            else {
                // 1. Error when creating a user
                print("ERROR!")
                print(error?.localizedDescription)
                
                // 2. Show the error in the UI
                self.statusMessageLabel.text = error?.localizedDescription
                
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("going to next page")
    }
    
}
